﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        string operatorSign;
        string number;
        double num1, num2;
        bool op = false;
      
        public MainPage()
        {
            InitializeComponent();

        }

        void OnNum(object sender, EventArgs e)
        {
            result.Text += ((Button)sender).Text;
            if(op)
            {
                number += ((Button)sender).Text;
            }
        }

        void OnOp(object sender, EventArgs e)
        {
            num1 = Convert.ToDouble(result.Text);
            operatorSign = ((Button)sender).Text;
            result.Text += ((Button)sender).Text;
            op = true;
            
        }

        void OnEqual(object sender, EventArgs e)
        {
            num2 = Convert.ToDouble(number);
            op = false;

            switch(operatorSign)
            {
                case "+":
                    result.Text = (num1 + num2).ToString();
                    break;
                case "-":
                    result.Text = (num1 - num2).ToString();
                    break;
                case "x":
                    result.Text = (num1 * num2).ToString();
                    break;
                case "/":
                    result.Text = (num1 / num2).ToString();
                    break;
            }

            num1 = 0;
            num2 = 0;
            number = "";


        }

        void OnClear(object sender, EventArgs e)
        {
            result.Text = "";
            num1 = 0;
            num2 = 0;
            number = "";
        }
    }
}
